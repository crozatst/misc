#!/bin/bash

# Resynx 0.5 sec every hour (3600s)
OFFSETPERSEGMENT=0.5
SEGMENT=3600

# Resync list
FILELIST=/tmp/_list_$1.txt

# Input movie duration
DURATION=`ffprobe -i $1 -show_format -v quiet | sed -n 's/duration=//p' | xargs printf %.0f`
LOOP=$(($DURATION/$SEGMENT))

# First segment do nothing
ffmpeg -y -i $1 -t $SEGMENT -c copy /tmp/_0_$1
echo "file /tmp/_0_$1" > $FILELIST

# Resync function
function cutAndResync { 
START=$(($SEGMENT + ($2 - 1) * $SEGMENT))
END=$(($START + $SEGMENT))
CUTFILE=/tmp/_$2_$1
RESYNCFILE=/tmp/_$2_resync_$1
OFFSET="0$(echo "$OFFSETPERSEGMENT * $2" | bc)"
if [ $DURATION -gt $START ]
then 
  ffmpeg -y -ss $START -i $1 -t $SEGMENT -c copy $CUTFILE
  ffmpeg -y -i $CUTFILE -itsoffset $OFFSET -i $CUTFILE -map 1:0 -map 0:1 -c copy $RESYNCFILE
  echo "file $RESYNCFILE" >> $FILELIST
fi
}

for i in `seq 1 $LOOP` 
  do cutAndResync $1 $i;
done

# Reassembly
ffmpeg -y -f concat -safe -0 -i $FILELIST -c copy resync_$1
